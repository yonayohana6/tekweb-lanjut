import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { KategoriComponent } from './pages/kategori/kategori.component';
import { KeranjangComponent } from './pages/keranjang/keranjang.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    KategoriComponent,
    KeranjangComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
